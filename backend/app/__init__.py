# Flask application instance

import os

from flask import Flask
from flask_sse import sse

app = Flask(__name__)

app.config["REDIS_URL"] = os.environ['REDISCLOUD_URL'] or "redis://localhost"
app.register_blueprint(sse, url_prefix='/stream')

from app import routes
