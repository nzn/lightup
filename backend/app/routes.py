# Home page route
from flask import Flask, request, render_template
from flask_sse import sse
from app import app
import random

def generateRandomColor():
    r = lambda: random.randint(0,255)
    return('#%02X%02X%02X' % (r(),r(),r()))

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    return render_template('index.html')

@app.route('/group', methods=['GET', 'POST'])
def group():
    if request.method == 'POST':
        if 'Create lightgroup' in request.form:
            max_id = 10000
            group_num = (f"{random.randint(max_id/10,max_id-1)}")
            group_message="Do you want to play?"
            return render_template('group.html', group_number = group_num, group_message=group_message)
        else:
            group_num = request.form["Given Number"]
            print(group_num)

            if 'light' in request.form:
                sse.publish({"message": "Hello!"}, type='greeting', channel = group_num)
                group_message= "Message sent!"
                return render_template('group.html', group_number = group_num, group_message=group_message)
                """ if 'backcolor' in request.form:
                sse.publish({"message": "Let's change the color!"}, type='changeColor', channel = group_num)
                group_message= "purple!"
                return render_template('group.html', group_number = group_num, group_message=group_message)  """
            elif 'randomcolor' in request.form:
                color = generateRandomColor()
                sse.publish({"message": color}, type='changeColorRandomly', channel = group_num)
                group_message= "Let's have Fun!!!"
                return render_template('group.html', group_number = group_num, group_message=group_message) 


@app.route('/join', methods=['GET', 'POST'])
def join():
        if request.method == 'POST':
            if 'Join lightgroup' in request.form:
                group_num = request.form['Given Number']
                return render_template('join.html', groupnumber = group_num)
