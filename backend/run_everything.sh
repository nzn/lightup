virtualenv -p python3 venv
source ./venv/bin/activate
pip install -r requirements.txt
./run_redis.sh
./run_webserver.sh
